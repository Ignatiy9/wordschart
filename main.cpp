#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "control.h"
#include "hystogramm.h"
#include <QThread>
#include <QQmlContext>
#include <QVariant>

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QApplication app(argc, argv);

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    Control *control = new Control();
    Hystogramm *hg = control->getHg();

    engine.rootContext()->setContextProperty("control", control);
    engine.rootContext()->setContextProperty("hg", hg);

    return app.exec();
}
