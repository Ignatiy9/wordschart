#include "wordsorter.h"
#include <QDebug>

WordSorter::WordSorter()
{

}

void WordSorter::addWord(const QString &word)
{
    auto pt = map_.find(word);
    if(pt != map_.end()){
        pt->second++;
    }
    else{
        map_.emplace(word, 1);
    }

    emit newTop(getTopWords());
}

TVec WordSorter::getTopWords()
{
    // Создаем вектор-копию дерева===================================
    QVector<TPair> v(map_.begin(), map_.end());
    vSort sortVector(&v);

    sortVector(NUM_SORT);
    v.resize(nWords);
    sortVector(ALPH_SORT);

    return v;
}

WordSorter::vSort::vSort(TVec *v)
{
    this->v = v;
}

void WordSorter::vSort::operator()(SortType sortType)
{
    std::sort(v->begin(), v->end(),
            [=] (TPair &a, TPair &b){
        return sortType == ALPH_SORT? a.first < b.first : a.second > b.second;
    });
}
