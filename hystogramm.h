#ifndef HYSTOGRAMM_H
#define HYSTOGRAMM_H

#include <QObject>

#include <QtCharts>
#include <QBarSeries>
#include <QBarSet>
#include <QBarCategoryAxis>
#include <QBarSet>
#include <QTimer>

#include "MyType.h"

class Hystogramm : public QObject
{
    Q_OBJECT

    QAbstractAxis *_wordsAxis = new QBarCategoryAxis;
    /// \brief Ось количества включений слов из ТОП-15
    QAbstractAxis *_countAxis = new QValueAxis;
    /// \brief Набор данных ТОП-15
    QHorizontalBarSeries *bs = new QHorizontalBarSeries();
    QBarSet *set = new QBarSet("set");

    QTimer t;

    TVec top;

public:
    explicit Hystogramm(QObject *parent = nullptr);

    Q_INVOKABLE QAbstractAxis *wordsAxis();
    /// \brief Возвращает указатель на ось значений (количесво включений слов ТОП-15 в файл)
    Q_INVOKABLE QAbstractAxis *countAxis();

    /// \brief Сохраняет указатель на гистограмму
    Q_INVOKABLE void setBarSeries(QHorizontalBarSeries*);


    bool hasNewTop = false;

    void setTop(const TVec &newTop);

signals:

};

#endif // HYSTOGRAMM_H
