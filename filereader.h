#ifndef FILEREADER_H
#define FILEREADER_H

#include <QObject>
#include <QFile>

class FileReader : public QObject
{
    Q_OBJECT
public:
    explicit FileReader(QObject *parent = nullptr);
    QString filePath;
    void setFilePath(const QVariant &newFilePath);
    void startParse();
    bool isSeparator();

private:
    QFile file;

signals:
    void readyToStart();
    void finished();
    void newLine(const QString &line);
    void progress(const float progress);
};

#endif // FILEREADER_H
