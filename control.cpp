#include "control.h"
#include "filereader.h"
#include "lineparser.h"
#include <QThread>
#include <QDebug>

Control::Control()
{
    file_reader = new FileReader;
    file_thread = new QThread;

    file_reader->moveToThread(file_thread);

    connect(file_thread, &QThread::started, file_reader, &FileReader::startParse);


    //******************************************

    line_parser = new LineParser();
    connect(file_reader, &FileReader::newLine, line_parser, &LineParser::parseLine);
    line_thread = new QThread;
    line_parser->moveToThread(line_thread);

    //******************************************

    word_sorter = new WordSorter;
    word_thread = new QThread;
    word_sorter->moveToThread(word_thread);

    connect(line_parser, &LineParser::newWord, word_sorter, &WordSorter::addWord);

    //******************************************

    hg = new Hystogramm;
    hg_thread = new QThread;
    hg->moveToThread(hg_thread);

    connect(word_sorter, &WordSorter::newTop, hg, &Hystogramm::setTop);

    //******************************************
    connect(file_reader, &FileReader::readyToStart, [=](){
        file_thread->start();
        line_thread->start();
        word_thread->start();
          hg_thread->start();
    });

    connect(file_reader, &FileReader::finished, [=](){
        file_thread->quit();
    });
}

Hystogramm *Control::getHg()
{
    return hg;
}

void Control::setFilePath(const QVariant &newFilePath)
{
    file_reader->setFilePath(newFilePath);
}
