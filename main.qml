import QtQuick 2.15
import QtQuick.Window 2.15
import Qt.labs.platform 1.0
import QtQuick.Controls 2.5
import QtCharts 2.3
import QtQuick.Layouts 1.3

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")

    ColumnLayout{
        anchors.fill: parent
        Loader{
            id: loader
            Layout.preferredHeight: 50
            Layout.fillWidth: true
            sourceComponent: button
        }
        Loader{
            id: content
            Layout.fillHeight: true
            Layout.fillWidth: true
            sourceComponent: hystogramm
        }
    }
    Component{
        id: button
        Button{
            id: openFileDialog
            onClicked: fileDialog.visible = true
        }
    }
    Component{
        id: hystogramm
        ChartView {
            id: chart
            legend.visible: true
            antialiasing: true

            Component.onCompleted: {
                var series = chart.createSeries(ChartView.SeriesTypeHorizontalBar);
                hg.setBarSeries(series)

                series.barWidth = 0.8;

                chart.setAxisX(hg.countAxis(), series)
                chart.setAxisY(hg.wordsAxis(), series)
            }
        }
    }

    FileDialog {
        id: fileDialog
        title: "Please choose a file"
        fileMode: FileDialog.OpenFile
        onAccepted: {
            control.setFilePath(fileDialog.file)
            content.sourceComponent = hystogramm
        }
        onRejected: close()
    }
}
