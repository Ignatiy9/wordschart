#ifndef LINEPARSER_H
#define LINEPARSER_H

#include <QObject>
#include "filereader.h"

class LineParser : public QObject
{
    Q_OBJECT
public:
    explicit LineParser();
    void parseLine(QString line);
    QString lastWord;
    bool isSeparator(QChar c);
signals:
    void newWord(const QString &word);
};

#endif // LINEPARSER_H
