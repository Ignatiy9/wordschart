#include "lineparser.h"
#include <QDebug>
#include <QRegularExpression>

LineParser::LineParser()
{

}

void LineParser::parseLine(QString line)
{
    line.replace('\n', ' ');
    if(!isSeparator(line[0])){
        line = lastWord + line;
        lastWord.clear();
    }
    QString nextWord;
    for(auto n : line){
        if(isSeparator(n)){
            if(!nextWord.isEmpty()){
                emit newWord(nextWord);
                qDebug() << nextWord;
                nextWord.clear();
            }
        }
        else{
            nextWord.append(n);
        }
    }
    lastWord = nextWord;
}

bool LineParser::isSeparator(QChar ch)
{
    return !(ch.isLetterOrNumber() || ch == '-');
}
