#include "hystogramm.h"

Hystogramm::Hystogramm(QObject *parent) : QObject(parent)
{
    t.setInterval(100);
//    t.start();
    connect(&t, &QTimer::timeout, this, [&](){
        qDebug() << Q_FUNC_INFO << "SHOW";

        if (top.isEmpty() || !hasNewTop)
            return;
        hasNewTop = false;

        for(auto a : top)
            qDebug() << a.first << " " << a.second;

        QVector<QString> categories;
        categories.reserve(top.count());

        int max = 0;
        int cnt = 0;

        for (TPair p : top){
            categories.append(p.first);
            set->replace(cnt++, p.second);

            if (max < p.second)
                max = p.second;
        }

        // Заполнение категорий и сетов ========================================
        _countAxis->setMax(max < 10 ? max : ceil(float(max) / 20) * 20);
        qobject_cast<QValueAxis*>(_countAxis)->setTickCount(max < 10 ? (max + 1) : 5);

        qobject_cast<QBarCategoryAxis*>(_wordsAxis)->setCategories(categories);
        _wordsAxis->setMin(categories.first());
        _wordsAxis->setMax(categories.last());

    }, Qt::DirectConnection);
}

QAbstractAxis *Hystogramm::wordsAxis()
{
    _wordsAxis->setMin(0);
    _wordsAxis->setMax(1);

//    qobject_cast<QBarCategoryAxis*>(_wordsAxis)->setLabelsAngle(270);

    return _wordsAxis;
}

QAbstractAxis *Hystogramm::countAxis()
{
    _countAxis->setMin(0);
    _countAxis->setMax(1);

    qobject_cast<QValueAxis*>(_countAxis)->setLabelFormat("%i");

    return _countAxis;
}

void Hystogramm::setBarSeries(QHorizontalBarSeries *s)
{
    bs = s;
    bs->append(set);
    bs->setLabelsVisible(true);
}

void Hystogramm::setTop(const TVec &newTop)
{
    top = newTop;
    hasNewTop = true;

    for(auto a : top)
        qDebug() << a.first << " " << a.second;

    QVector<QString> categories;
    categories.reserve(top.count());

    int max = 0;
    int cnt = 0;

    for (TPair p : top){
        categories.append(p.first);
        set->replace(cnt++, p.second);

        if (max < p.second)
            max = p.second;
    }

    // Заполнение категорий и сетов ========================================
    _countAxis->setMax(max < 10 ? max : ceil(float(max) / 20) * 20);
    qobject_cast<QValueAxis*>(_countAxis)->setTickCount(max < 10 ? (max + 1) : 5);

    qobject_cast<QBarCategoryAxis*>(_wordsAxis)->setCategories(categories);
    _wordsAxis->setMin(categories.first());
    _wordsAxis->setMax(categories.last());

}
