#ifndef MYTYPE_H
#define MYTYPE_H

#endif // MYTYPE_H

#include <iostream>
#include <map>
#include <QString>

typedef std::pair<QString, int> TPair;
typedef std::map <QString, int> TMap;
typedef          QVector<TPair> TVec;
