#include "filereader.h"
#include <fstream>
#include <iostream>
#include <QVariant>
#include <QString>
#include <QtConcurrent>
#include <QFuture>

#define MAX_BYTES 8 * 1024

using namespace std;

FileReader::FileReader(QObject *parent) : QObject(parent)
{

}

void FileReader::setFilePath(const QVariant &newFilePath)
{
    filePath = newFilePath.toString().split(':').at(1);
    emit readyToStart();
}

bool FileReader::isSeparator()
{
    return 1;
}

void FileReader::startParse()
{
    qDebug() << "*********START PARSING********";
    file.setFileName(filePath);
    if (!file.open(QIODevice::ReadOnly))
        return;

    quint64 fBytes = file.size();
    quint64 rBytes = 0;

    QTextStream out(&file);

    QString line;
    while (out.readLineInto(&line, MAX_BYTES)) {
        rBytes += line.toUtf8().size();
        if(line.size() < MAX_BYTES)
            line += ' ';
        emit newLine(line);
//        emit progress((float)rBytes / (float)fBytes);
    }
    file.close();
    qDebug() << "*********FINISH PARSING********";
//    emit finished();
}

