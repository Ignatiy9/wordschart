#ifndef CONTROL_H
#define CONTROL_H

#include <QObject>
#include "MyType.h"
#include "filereader.h"
#include "lineparser.h"
#include "wordsorter.h"
#include "hystogramm.h"
#include <QThread>

#include <QtCharts>
#include <QBarSeries>
#include <QBarSet>
#include <QBarCategoryAxis>


class Control : public QObject
{
    Q_OBJECT

public:
    explicit Control();

    FileReader *file_reader;
    QThread    *file_thread;

    LineParser *line_parser;
    QThread    *line_thread;

    WordSorter *word_sorter;
    QThread    *word_thread;

    Hystogramm *hg;
    QThread    *hg_thread;
    Hystogramm *getHg();

public slots:
    void setFilePath(const QVariant &newFilePath);
    //file_reader
    //word_picker
    //~~barset_maker <- это процедура, выполняемая в future)

signals:

};

#endif // CONTROL_H
