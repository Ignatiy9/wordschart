#ifndef WORDSORTER_H
#define WORDSORTER_H

#include <QObject>
#include "MyType.h"

class WordSorter : public QObject
{
    Q_OBJECT
    enum SortType{
        ALPH_SORT,
        NUM_SORT
    };
#define STNDRT_NWORDS 15

public:
    explicit WordSorter();

    TVec getTopWords();
    void addWord(const QString &word);

    void setCountTopWords(int count);

    class vSort{
        public:
        vSort(TVec *v);
        TVec *v;
        void operator()(SortType sortType);
    };

private:
    TMap map_;
    int nWords = STNDRT_NWORDS;

signals:
    void newTop(TVec topWords);
};

#endif // WORDSORTER_H
